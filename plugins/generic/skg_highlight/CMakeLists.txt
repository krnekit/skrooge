#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_HIGHLIGHT ::..")

PROJECT(plugin_highlight)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skg_highlight_SRCS
	skghighlightplugin.cpp)

KCOREADDONS_ADD_PLUGIN(skg_highlight SOURCES ${skg_highlight_SRCS} INSTALL_NAMESPACE "skg_gui" JSON "metadata.json")
TARGET_LINK_LIBRARIES(skg_highlight KF5::Parts skgbasemodeler skgbasegui)

########### install files ###############
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skg_highlight.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skg_highlight )
